import {writable,derived} from "svelte/store";

export const apiData = writable([]);
export const bidList = writable([]);

export const token = writable(localStorage.getItem('token'))
export const userId = writable(localStorage.getItem('id'));
export const role = writable(localStorage.getItem('role'));
