import { bidList} from "../stores/stores";
let bids


export async function removeBid(bidId){
    return await fetch(`http://localhost:3000/bids/`+bidId, {
        method: 'DELETE',
        headers:{
            'Content-Type':'application/json',
            'Authorization':'Bearer' +''+localStorage.getItem('token')
        }
    })
        .then((response) => response.json())
        .then((data) => {
            return data;
        });
}
export async function getSelectBids(userId) {
    const resp = await fetch('http://localhost:3000/users/' + userId+'/bids')
    bids = await resp.json();

    bidList.update((oldValue) => {
        return bids;
    });
}
