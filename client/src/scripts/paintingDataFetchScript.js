import {apiData, token} from "../stores/stores.js"; //

let paintings;
let users;
let response;
let record;
let bids;
let highestBid

let unsubscribe = apiData.subscribe(value => {
    paintings = value;
});


async function getOnePainting() {
    // this will also return null
    return await fetch('http://localhost:3000/paintings')
        .then((response) => response.json());
}






async function loadPaintings(){
    const resp = await fetch('http://localhost:3000/paintings');
    paintings = await resp.json();

    apiData.update((oldValue) => {
        return paintings;
    });
}
async function getQueryItems(startprice){
    const resp = await fetch('http://localhost:3000/paintings?startprice='+startprice);
    paintings = await resp.json();

    apiData.update((oldValue) => {
        return paintings;
    })
}
async function getQueryItemsStyle(style){
    const resp = await fetch('http://localhost:3000/paintings?style='+style);
    paintings = await resp.json();

    apiData.update((oldValue) => {
        return paintings;
    });
}

async function getQueryItemsAuthor(author){
    const resp = await fetch('http://localhost:3000/paintings?author='+author);
    paintings = await resp.json();

    apiData.update((oldValue)=> {
        return paintings;
    });
}



 async function placeBidScript(bid,paintingId,name,userId){
    try {

        record = {
            amount:bid,
            painting:name
        }

        response = await fetch('http://localhost:3000/bids/' + userId+"/"+paintingId,{
            method: 'POST',
            headers:{
                'Content-Type': 'application/json',
                'Authorization': 'Bearer' +''+ localStorage.getItem('token')
            },
            body:JSON.stringify(record)
        });

        return await response.json()

    }catch (e){
        throw new Error(response.json())
    }

}


export {loadPaintings, getOnePainting,getQueryItems,getQueryItemsStyle,getQueryItemsAuthor,placeBidScript}