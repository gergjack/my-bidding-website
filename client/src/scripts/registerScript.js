import jwt_decode from "jwt-decode";


let response;

async function registerUser(username,email,password){

    try {

        let newUser = {
            username:username,
            roles:[],
            email:email,
            password:password
        }
         response = await fetch('http://localhost:3000/users',{
            method:'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newUser)
        })
        return await response.json();
    }catch (e){
        throw new Error(response.json())
    }

}
async function logIn(username,password){
    try{


        let body = {
            username:username,
            password:password
        }
        response = await fetch('http://localhost:3000/tokens',{
            method:'POST',
            headers:{
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        });
        return await response.json();
    }catch (e){
        throw new Error(response.json())

    }
}
export function decodeToken(token){
    return jwt_decode(token);
}


export {registerUser,logIn}