
export async function deletePainting(id) {

    console.log(id)
    return await fetch('http://localhost:3000/paintings/' + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + '' + localStorage.getItem('token')
        }
    })

}

export async function addPainting(painting){
    return await fetch('http://localhost:3000/paintings', {
        method: 'POST',
        headers: {
            'Content-Type':'application/json',
            'Authorization':'Bearer' +''+localStorage.getItem('token')
        },
        body: JSON.stringify({
            name:painting.name,
            author:painting.author,
            style:painting.style,
            picture:painting.picture,
            startprice: painting.startprice,
            startdate:painting.startdate,
            enddate:painting.enddate,
            Participants:[],
            highestBid:[]
        })
    });

}
