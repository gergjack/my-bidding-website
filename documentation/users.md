# Users

Place the credentials for at least two users in the table below. There should be at least one administrator and one regular user.


| Username | Password | Account type |
|----------|--|------------|
| `Noob`  |1234 | user|
| `Gerald`   | 1234| admin|


