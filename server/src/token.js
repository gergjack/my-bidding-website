import express from "express";
import {findUserByUsername} from "./utils/userUtils.js";
import userData from "./data/userData.js";
import bcrypt from "bcrypt";
import jwt  from "jsonwebtoken";
import secret from "./secret.js";

const tokenRouter = express.Router();

tokenRouter.post('/', function (req, resp) {
    const {username, password} = req.body;
    let token = {};
    if (!(username && password)) {
        return resp.status(400).json({error: "All input is required to log in"})
    }

    const foundUser = findUserByUsername(userData, username);
    if (foundUser === undefined) {
        return resp.status(404).json({error: 'The user could not be found'})
    }

    let payload = {
        id: foundUser.id,
        username: foundUser.username,
        roles: [foundUser.roles]

    }
    let header = {
        algorithm: 'HS256'
    }

    bcrypt.compare(password, foundUser.password, (error, result) => {
        if (error) {
            return resp.status(500).json({error: 'Something went wrong while comparing passwords'});
        }

        if (!result) {
            return resp.status(401).json({error: 'Invalid Credentials'})
        }

        jwt.sign(payload, secret, header, (error, result) => {
            if (error) {
                return resp.status(500).json({error: 'Something went wrong while creating a token '});
            }
            req.token = token;
            return resp.status(201).json({id:foundUser.id,token: result,roles:[foundUser.roles]});
        });
    });
});

export default tokenRouter;