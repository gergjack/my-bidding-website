import express from 'express';
import userRouter from './routes/userScript.js'
import bidRouter from "./routes/bidScript.js";
import paintingRouter from "./routes/paintingScript.js";
import tokenRouter from "./token.js";



import cors from 'cors';
const port = 3000

const app = express();
app.use(cors());
app.use(express.json());
app.use('/users',userRouter);
app.use('/bids',bidRouter);
app.use('/paintings',paintingRouter);
app.use('/tokens',tokenRouter);

app.get('/', (req, res) => {
  console.log(req);
  // res.send('Hello World!')
  res.json({ msg: "hello world"});
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
