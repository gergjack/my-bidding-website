import express from "express";
import {
    findPaintingInListByID,
    getDesignator,
    postPainting,
    patchPainting,
    deletePainting,
} from "../middleware/painting-middleware.js";
import {getListOfParticipants, paintingInList} from "../utils/paintingUtils.js";
import paintingData from "../data/paintingData.js";
import {isLoggedIn} from "../middleware/signUp-middleware.js";


const paintingRouter = express.Router();

paintingRouter.get('/',getDesignator,function (req,resp){
        return resp.status(200).json(req.query);
});

paintingRouter.get('/:id',findPaintingInListByID,function (req,resp){
    return resp.status(200).json(req.foundPainting);
});


paintingRouter.post('/',isLoggedIn,postPainting,function (req,resp){
    return resp.status(201).json(req.returnBody);
});


paintingRouter.patch('/:id',isLoggedIn,findPaintingInListByID,patchPainting,function (req,resp){
    return resp.status(200).json(req.updatedPainting);
});


paintingRouter.put('/:id',findPaintingInListByID,function (req,resp){

    const foundPainting = req.foundPainting;
    const pathId = req.pathId;
    const index = paintingData.indexOf(foundPainting);
    paintingData.splice(index,1);
    const newPainting = req.body;
    newPainting.id = pathId;
    paintingData.push(newPainting);
    return resp.status(200).json(newPainting);
});

paintingRouter.delete('/:id',isLoggedIn,findPaintingInListByID,deletePainting,function (req,resp){
   return resp.status(200);
});


paintingRouter.delete('/:id/users',findPaintingInListByID,function (req,resp){
   const foundPainting = req.foundPainting;
   let list = getListOfParticipants(foundPainting);
   list.splice(0,list.length);
   return resp.status(200);
});




export default paintingRouter;