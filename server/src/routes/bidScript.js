import express from "express";
import bidData from '../data/bidData.js'
import {isLoggedIn} from "../middleware/signUp-middleware.js";
import {
    bidDesignator, checkValid,
    deleteBid,
    findBidInListById,
    patchBid,
    postBid,

} from "../middleware/bid-middleware.js";

const bidRouter = express.Router();

bidRouter.get('/', bidDesignator,function (req, resp) {
    return resp.status(200).json(req.query);
});

bidRouter.get('/:id', findBidInListById, function (req, resp) {
    return resp.status(200).json(req.foundBid);

});



bidRouter.post('/:id/:paintingId',isLoggedIn,checkValid,postBid, function (req, resp) {
    return resp.status(201).json(req.newBid);

});



bidRouter.patch('/:id',isLoggedIn,findBidInListById, patchBid, function (req, resp) {
    return resp.status(200).json(req.updatedBid);
});

bidRouter.put('/:id',isLoggedIn, findBidInListById,function (req, resp) {
  let foundBid = req.foundBid;
  const pathId = req.pathId;
  const index = bidData.indexOf(foundBid);
  bidData.splice(index,1);
  const newBid = req.body;
  newBid.id = pathId;
  bidData.push(newBid);
  return resp.status(200).json(newBid);
});
bidRouter.delete('/:id',isLoggedIn,deleteBid, function (req, resp) {
    return resp.status(200).json({complete:"Done"});
});
export default bidRouter;

