import express from "express";
import userData from '../data/userData.js';
import {getListOfRolesFromId, getListOfBidsFromId,} from '../utils/userUtils.js';
import {deleteUser,findInListById,getDesignator,patchUser, postUser }from "../middleware/user-middleware.js";
import {isLoggedIn} from "../middleware/signUp-middleware.js";



const userRouter = express.Router();

userRouter.get('/',getDesignator, function (req, resp) {
    return resp.status(200).json(req.query);
});

userRouter.get('/:id', findInListById, function (req, resp) {
    return resp.status(200).json(req.foundUser);

});
userRouter.get('/:id/roles', findInListById, function (req, resp) {
    const id = req.pathId;
    return resp.status(200).json(getListOfRolesFromId(id, userData));

});
userRouter.get('/:id/bids', findInListById, function (req, resp) {
    const id = req.pathId;
    return resp.json(getListOfBidsFromId(id, userData));

})

userRouter.post('/', postUser, function (req, resp) {
    return resp.status(201).json(req.returnBody);
});

userRouter.put('/:id', findInListById, function (req, resp) {

    let foundUser = req.foundUser;
    const pathId = req.pathId;
    const index = userData.indexOf(foundUser);
    userData.splice(index, 1);
    const newUser = req.body;
    newUser.id = pathId;
    userData.push(newUser);
    return resp.status(200).json(newUser);

});

userRouter.patch('/:id',isLoggedIn,findInListById, patchUser, function (req, resp) {
    return resp.status(200).json(req.updatedBody);


});

userRouter.delete('/:id',isLoggedIn,findInListById,deleteUser, function (req, resp) {
    return resp.status(200);

});
userRouter.delete('/:id/bids', findInListById, function (req, resp) {
    const id = req.pathId;
    let list = getListOfBidsFromId(id, userData);
    list.splice(0, list.length);
    return resp.status(200);
});





export default userRouter;
