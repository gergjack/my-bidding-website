import userData from "../data/userData.js";
import {findHighestId, getObjectFromId} from "../utils/dataUtils.js";
import {checkForDuplicatesInBidHistory, getListOfBidsFromId, userIsInList} from "../utils/userUtils.js";
import bcrypt from "bcrypt";
import {checkBidIdentity} from "../utils/bidUtils.js";
import bidData from "../data/bidData.js";


export function findInListById(req, resp, next) {
/// import function and parse id
    let pathId = Number.parseInt(req.params.id);
    const foundUser = getObjectFromId(userData, pathId);

    if (foundUser === undefined) {
        return resp.status(404).json({error: "The id you inputted could not be found! "});
    }
    req.foundUser = foundUser;
    req.pathId = pathId;
    return next();

}


export function postUser(req, resp, next) {

    let newUser = req.body;

    newUser.id = findHighestId(userData) + 1;
    newUser.roles = ['user'];
    newUser.bidhistory=[];
    const matchingUser = userIsInList(userData, newUser);


    if (matchingUser === undefined) {
        const passwordPlain = newUser.password;

       bcrypt.hash(passwordPlain, 10, function (err, hash) {
            newUser.password = hash;
            userData.push(newUser);
           req.newUser = newUser;
           req.returnBody = {
               id:newUser.id,
               username: newUser.username,
               password: newUser.password
           };
           return next();
        });

    } else {
        return resp.status(400).json({error: "The user already exists!"})
    }

}

export function getDesignator(req, resp, next) {
    let list = userData;

    if (req.query.email) {
        list = userData.filter(item => item.email === req.query.email);

    } else if (req.query.username) {
        list = userData.filter(item => item.username === req.query.username);

    }
    req.query = list;
    return next();
}

export function deleteUser(req, resp, next) {
        let pathId = Number.parseInt(req.params.id);
        const toDelete = getObjectFromId(userData, pathId);
        const index = userData.indexOf(toDelete);
        userData.splice(index, 1);
        return next();

}

export function patchUser(req, resp, next) {
    let pathId = Number.parseInt(req.params.id);
    if ('id' in req.body) {
        return resp.status(400).json({error: "Its not allowed to change id of a user"});

    } else {
        let updateBody = req.body;
        let foundBody = getObjectFromId(userData, pathId);
        const index = userData.indexOf(foundBody);

        updateBody = Object.assign({}, foundBody, updateBody);
        userData.splice(index, 1);
        userData.push(updateBody);
        req.updatedBody = updateBody;
        return next();
    }
}

export function validateBid(req, resp, next) {
    let bid = req.body;
    req.bid = bid;

    if (JSON.stringify(checkBidIdentity(bidData, bid)) === JSON.stringify(bid)) {
        return next();
    } else {
        return resp.status(404).json({error: 'To add a bid to a user the bid must be present in the database'});
    }

}

export function postUserBid(req, resp, next) {
    const id = req.pathId;
    let list = getListOfBidsFromId(id, userData);
    let bid = req.bid;
    const bidID = Number.parseInt(bid.id);

    if (checkForDuplicatesInBidHistory(list, bidID) === undefined) {
        list.push(bid);
    } else {
        return resp.status(400).json({error: 'This bid is already present in the list!'})
    }
    return next();
}



