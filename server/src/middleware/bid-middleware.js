import bidData from "../data/bidData.js";
import {findHighestId, getObjectFromId} from "../utils/dataUtils.js";
import {bidIsInList, getPaintingFromBid} from "../utils/bidUtils.js";
import userData from "../data/userData.js";
import {
    checkForDuplicatesInParticipants,
    checkPaintingIdentity, findPaintingByName,
    getListOfParticipants
} from "../utils/paintingUtils.js";
import paintingData from "../data/paintingData.js";
import {getListOfBidsFromId} from "../utils/userUtils.js";


export function findBidInListById(req, resp, next) {
    let pathId = Number.parseInt(req.params.id);
    const foundBid = getObjectFromId(bidData, pathId);

    if (foundBid === undefined) {
        return resp.status(404).json({error: "The id you inputted could not be found! "});
    }
    req.foundBid = foundBid;
    req.pathId = pathId;
    return next();
}
export function bidDesignator(req,resp,next){
    let list = bidData;
    if (req.query.userId){
        list = bidData.filter(item => Number.parseInt(item.userId) === Number.parseInt(req.query.userId))
    }
    else if (req.query.painting){
        list = bidData.filter(item => item.painting === req.query.painting);
    }
    req.query = list;
    return next();
}

export function postBid(req, resp, next) {

    const newBid = req.body;
    const userID = Number.parseInt(req.params.id);
    newBid.userId = userID;


    newBid.id = findHighestId(bidData) + 1;
    req.newBid = newBid;
    const matchingBid = bidIsInList(bidData, newBid);

    const User = req.user;
    let Painting = req.painting;




    if (matchingBid === undefined) {
        newBid.painting = Painting.name
        bidData.push(newBid);
        Painting.highestBid.push(newBid.amount)
        let list = Painting.Participants;
        if (checkForDuplicatesInParticipants(list,userID) === undefined){
            Painting.Participants.push(User);
            let usersBids = User.bidhistory;
            usersBids.push(newBid);
        }
        return next();
    } else {
        return resp.status(400).json({error: "The bid already exists "});
    }

}

export function patchBid(req, resp, next) {
    let pathId = Number.parseInt(req.params.id);
    if ('id' in req.body) {
        return resp.status(400).json("Its not allowed to change id of a object");
    } else {
        let updateBid = req.body;
        let foundBid = getObjectFromId(bidData, pathId);
        const index = bidData.indexOf(foundBid);

        updateBid = Object.assign({}, foundBid, updateBid);
        bidData.splice(index, 1);
        bidData.push(updateBid);


        //for user list
        req.updatedBid = updateBid;
        return next();
    }
}

export function deleteBid(req, resp, next) {
    /// Delete main
        let pathId = Number.parseInt(req.params.id);
        const toDelete = getObjectFromId(bidData, pathId);
        const index = bidData.indexOf(toDelete);
        bidData.splice(index, 1);

        ///Delete user side
        let userID = toDelete.userId;
       let list = getListOfBidsFromId(userID,userData);
       const bidIndex  = list.indexOf(toDelete);
       list.splice(bidIndex,1);



       ///Delete from participants && highestbid stack
       let painting = findPaintingByName(toDelete.painting);
       let participants = painting.Participants;
       let user = getObjectFromId(userData,userID);
       const userIndex = participants.indexOf(user)
        participants.splice(userIndex,1);

       let bets = painting.highestBid;
       bets.splice(bets.length-1);


        return next();

}

export function validatePainting(req, resp, next) {
    let painting = req.body;


    if (JSON.stringify(checkPaintingIdentity(paintingData, painting)) === JSON.stringify(painting)) {
        return next();
    } else {
        return resp.status(404).json({error: 'To add a painting reference to a bid, it must be present in the database!'});
    }
}

export function checkIfHasPainting(req, resp, next) {
    let foundBid = req.foundBid;
    let painting = getPaintingFromBid(foundBid);

    if (Object.keys(painting).length === 0) {
        foundBid.Painting = req.body;
    } else {
        return resp.status(400).json({error: 'This bid is already referenced by a painting!'})
    }
    return next();
}
export function checkValid(req, resp, next) {
    const userID = Number.parseInt(req.params.id);
    const paintingId = Number.parseInt(req.params.paintingId)

    const User = getObjectFromId(userData,userID);
    const Painting = getObjectFromId(paintingData,paintingId);
    req.user = User;
    req.painting = Painting;


    if (Number.parseInt(req.body.amount) < Number.parseInt(Painting.highestBid)){
        return resp.status(400).json({error:"The amount of the bid cannot be less than the highest bid"});
    }

    if (Painting === undefined){
        return resp.status(404).json({error:"The id of the painting you provided is not valid"})

    }
    if (User === undefined){
        return resp.status(404).json({error:"The user could not be identified"})
    } else {
        return next();
    }

}