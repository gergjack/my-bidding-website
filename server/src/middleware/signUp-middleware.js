import jwt from 'jsonwebtoken';
import secret from "../secret.js";


export function isLoggedIn(req, resp, next){

    let header = req.headers.authorization.split('Bearer');
    const token = header[1];

    if (!token){
        return resp.status(403).json({error:'No request credentials sent!'});
    }

    jwt.verify(token,secret,(error) => {
        if (error){

            return resp.status(401).json({error:'Token is invalid'});

        } else {
            return next()
        }


    });

}

