import paintingData from "../data/paintingData.js";
import userData from "../data/userData.js";
import {findHighestId, getObjectFromId} from "../utils/dataUtils.js";
import { paintingInList} from "../utils/paintingUtils.js";
import {checkUserIdentity} from "../utils/userUtils.js";
import bidData from "../data/bidData.js";


export function findPaintingInListByID(req, resp, next) {
    let pathId = Number.parseInt(req.params.id);
    const foundPainting = getObjectFromId(paintingData, pathId);

    if (foundPainting === undefined) {
        return resp.status(404).json({error: "The id you inputted could not be found!"});
    } else {
        req.foundPainting = foundPainting;
        req.pathId = pathId;
        return next();
    }

}

export function postPainting(req, resp, next) {
    let newPainting = req.body;

    newPainting.id = findHighestId(paintingData) + 1;

    const matchingPainting = paintingInList(paintingData, newPainting);


    if (matchingPainting === undefined) {
        newPainting.highestBid = [newPainting.startprice];
        paintingData.push(newPainting);
        req.newPainting = newPainting;
        req.pathId = newPainting.id;
        return next();

    } else {
        return resp.status(400).json({error: "The painting already exists!"})
    }
}

export function getDesignator(req, resp, next) {
    let list = paintingData;

    if (req.query.author) {

        list = paintingData.filter(item => item.author === req.query.author);
    } else if (req.query.style) {
        list = paintingData.filter(item => item.style === req.query.style);
    } else if (req.query.startprice) {
        list = paintingData.filter(item =>  item.startprice === Number.parseInt(req.query.startprice));
    }
    req.query = list;
    return next();
}

export function validateUser(req, resp, next) {
    let user = req.body;

    if (JSON.stringify(checkUserIdentity(userData, user)) === JSON.stringify(user)) {
        req.user = user;
        return next();
    } else {
        return resp.status(404).json({error: 'To add a user to a participants list he must be already be present in the database!'})
    }
}



export function patchPainting(req, resp, next) {
    let pathId = Number.parseInt(req.params.id);
    let updateBody = req.body;
    let foundPainting = getObjectFromId(paintingData, pathId);

    if ('id' in req.body) {
        return resp.status(400).json({error: "Its not allowed to change id of the painting"});

    } else {


        const index = paintingData.indexOf(foundPainting);
        updateBody = Object.assign({}, foundPainting, updateBody);
        paintingData.splice(index, 1);
        paintingData.push(updateBody);
        req.updatedPainting = updateBody;
        return next();
    }
}

export function deletePainting(req,resp,next){
    let pathId = req.pathId;
    const toDelete = getObjectFromId(paintingData,pathId);
    const index = paintingData.indexOf(toDelete);
    paintingData.splice(index,1);
    return next();
}

