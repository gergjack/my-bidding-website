export default [

    {
        username: "Ceasar",
        roles: [
            "admin"
        ],
        email: "123@gmail.com",
        password: "$2b$10$84xbljTx9YqBymi.acji2e12VCF3VxIAFkuF4S1bQTGhzimSI0X6a",
        id: 1,
        bidhistory:[]
    },
    {
        username: "Gerald",
        roles: [
            "admin"
        ],
        email: "iloveleagueoflegends@gmail.com",
        password: "$2b$10$jZTNibGrNzkJsua5JEAa..ztJCpgsUGkE5g5QxHHOgqnbDANXvbFW",
        id: 2,
        bidhistory:[]


    },
    {
        username: "Noob",
        roles: [
            "user"
        ],
        email: "ilovenerf123@gmail.com",
        password: "$2b$10$jZTNibGrNzkJsua5JEAa..ztJCpgsUGkE5g5QxHHOgqnbDANXvbFW",
        id: 3,
        bidhistory:[]

    }




]

