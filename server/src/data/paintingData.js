export default [

    {
         id:1,
         name:"The Scream",
        author:'Edward Munch',
        picture:"https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Edvard_Munch%2C_1893%2C_The_Scream%2C_oil%2C_tempera_and_pastel_on_cardboard%2C_91_x_73_cm%2C_National_Gallery_of_Norway.jpg/1200px-Edvard_Munch%2C_1893%2C_The_Scream%2C_oil%2C_tempera_and_pastel_on_cardboard%2C_91_x_73_cm%2C_National_Gallery_of_Norway.jpg",
        startprice: 100000,
        startdate: "12/08/2022",
        enddate: "06/12/2022",
        style: "Expressionism",
        Participants:[],
        highestBid: [105000]
    },
    {
        id:2,
        name:"Black Square",
        author:'Kazimir Malevich',
        picture:'https://static.vecteezy.com/system/resources/previews/001/209/957/non_2x/square-png.png',
        startprice: 2000,
        startdate:"02/04/2022",
        enddate:"28/07/2022",
        style: "Abstract",
        Participants:[

            {
                id: 2,
                roles: ["admin", "user"],
                username: 'George Washington',
                email: 'Bigdaddy@hotwheels.com',
                password: 'dolabillz',
                bidhistory: []
            }
        ],
        highestBid: [2000]
    },
    {
        id:3,
        name:"Mona Lisa",
        author:'Leonardo DaVinci',
        picture:'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg/1200px-Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg',
        startprice: 1000,
        startdate:"20/08/2022",
        enddate:"20/09/2022",
        style: "Renaissance",
        Participants:[
            {
                id: 10,
                roles: ["admin", "user"],
                username: 'SnoopDog',
                email: 'Snoop6969@hotmales.com',
                password: '123IliketoEatGlue',
                bidhistory:[]
            }
        ],
        highestBid:[1000]
    },
    {
        id:4,
        name:"American Gothic",
        author:'Grant Wood',
        picture:'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/Grant_Wood_-_American_Gothic_-_Google_Art_Project.jpg/1200px-Grant_Wood_-_American_Gothic_-_Google_Art_Project.jpg',
        startprice: 10000,
        startdate:"20/08/2022",
        enddate:"20/09/2022",
        style: "Modernism",
        Participants: [],
        highestBid:[10000]
    },


]