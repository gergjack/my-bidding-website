import express from "express";

function getListOfRolesFromId(id, data) {
    let  roles = []
    data.forEach(item => {
        if (item.id === id) {
            roles = item.roles;
            return roles;
        }

    });
    return roles;
}

function getListOfBidsFromId(id, data) {
    let bids = []
    data.forEach(item => {
        if (item.id === id) {
            bids = item.bidhistory;
            return bids;
        }
    });
    return bids;
}
function checkUserIdentity(data,user){
    return data.find(item => JSON.stringify(item) === JSON.stringify(user));
}
function checkForDuplicatesInBidHistory(bidhistory,bidID){
  return bidhistory.find(item => item.id === bidID);
}

function userIsInList(data, body) {
    return data.find(item => item.username === body.username);
}
function findUserByUsername(data,username){
    return data.find(item => item.username === username);
}

export {getListOfRolesFromId, userIsInList, getListOfBidsFromId,checkUserIdentity,checkForDuplicatesInBidHistory,findUserByUsername};