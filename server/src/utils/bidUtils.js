function getPaintingFromBid(body){
    return body.Painting;
}
function bidIsInList(data,bid){


    return data.find(item => item.amount === Number.parseInt(bid.amount)  && item.userId === Number.parseInt(bid.userId) && item.painting == bid.painting );
}
function checkBidIdentity(data, body){
   return data.find(item => JSON.stringify(item) === JSON.stringify(body));

}

export {getPaintingFromBid,bidIsInList,checkBidIdentity};