import paintingData from "../data/paintingData.js";
function paintingInList (data,painting){
     return data.find(item => item.name === painting.name);
}
function getListOfParticipants(painting){
     return painting.Participants;
}
function findPaintingByName(paintingName){
     return paintingData.find(painting => painting.name === paintingName)
}
function checkPaintingIdentity(data,painting){

     return data.find(item => JSON.stringify(item) === JSON.stringify(painting));
}
function checkForDuplicatesInParticipants(participantsList, userID){
     return participantsList.find(item => item.id === userID);
}
export {paintingInList,getListOfParticipants,checkForDuplicatesInParticipants,checkPaintingIdentity,findPaintingByName};